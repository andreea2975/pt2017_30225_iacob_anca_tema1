package main;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class Convertire {

	public static Polinom extract(String s)
	{
		Double[] sir = new Double[500];
		Polinom p = new Polinom();
		String date = s;	
		Double x;
		int c = 0;
		char y ;

		
		ArrayList<String> termeni = new ArrayList<String>();
		Pattern pattern = Pattern.compile("(-?[0-9][0-9]*)");
		Matcher matcher = pattern.matcher(date);
		while (matcher.find())
				termeni.add(matcher.group(1));
		for ( String i:termeni )
		{
			y = i.charAt(0);
			if ( y == '-' ) 
			{
				i=i.replace('-', '0');
				x = Double.parseDouble(i);
				x *= -1;
			}
			else 
				x = Double.parseDouble(i);
			sir[c] = x;
			c++;
		}

		if ( c % 2 != 0 )
		{
			sir[c] = 0.0;
			c++;
		}
		for ( int j=0; j<c; j=j+2 )
			p.addTermeni( new Monom( sir[j],(sir[j+1]).intValue()  ) );
		System.out.println(p);
		return p;
	}
}
