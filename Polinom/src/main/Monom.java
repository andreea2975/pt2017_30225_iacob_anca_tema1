package main;

public class Monom {
	
	public double coeficient;
	int putere;
	
	public Monom(double coeficient,int putere)
	{
		
		this.coeficient=coeficient;
		this.putere=putere;
		
	}
	public String toString()
	{
		if(this.putere>0)
			if(this.coeficient<0)
			{
				return coeficient+"X"+putere;
			}
			else
			{
				return "+"+ coeficient+"X"+putere;
			}
		else
			if(coeficient>0)
			{
				return "+"+ coeficient+"X"+putere;
			}
		else  return coeficient+"X"+putere;
				
	}
	
}
