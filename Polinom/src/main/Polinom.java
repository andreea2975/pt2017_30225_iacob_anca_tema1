package main;
//import java.awt.List;
//import java.util.ArrayList;
import java.util.*;

public class Polinom {

	public String[] s = new String[500];
	public int a = 0;
	public double maxim = 0;
	public List <Monom> termeni;

	public Polinom() {

		termeni = new ArrayList<Monom>();
	}

	public void addTermeni(Monom m)
	{
		boolean ok = false;
		for (Monom m1 : termeni)
		{
			if (m1.putere == m.putere)
			{
				m1.coeficient += m.coeficient;
				ok = true;
				break;
			}
		}
		if (ok == false)
		{
			termeni.add(m);
		}
	}

	public String toString()
	{
		String y = "";
		for (Monom m1 : termeni) {
			if (m1.putere>maxim) {
				maxim = m1.putere;
				a++;
				for (int i = a;i >= 1;i--) {
					s[i + 1] = s[i];
				}
				s[1] = m1.toString();
			}
			else {
				a++;
				s[a] = m1.toString();
			}
		}
		for (int i = 1;i <= a;i++) {
			y = y + " " + s[i] + " ";
		}
		return y;
	}

	public static double coeficientmaxim(Polinom p, int puterea)
	{
		double x = 0.0;
		for (Monom m1 : p.termeni)
		{
			if (m1.putere == puterea)
			{
				x = m1.coeficient;
				break;
			}
		}
		return x;
	}
	public static int puteremaxim(Polinom p)
	{
		int x = 0, maxim = 1;
		for (Monom m1 : p.termeni)
		{
			if (m1.putere >= maxim)
			{
				maxim = m1.putere;
				x = m1.putere;
			}
		}
		return x;
	}

	public Polinom copy() {
		Polinom p = new Polinom();
		for (Monom m1 : termeni)
			if (m1.coeficient != 0) {
				p.addTermeni(new Monom(m1.coeficient, m1.putere));
			}
		return p;
	}

}
