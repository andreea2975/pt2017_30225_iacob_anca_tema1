package main;
import java.util.ArrayList;
import java.util.regex.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller{

	private Gui gui;
	public Polinom rezultat;

	public Controller(Gui g) {
		this.gui = g;

		setupControl();
	}

	public void setupControl() {
		ActionListener act = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String imput1="";
				String imput2="";
				Polinom p1=new Polinom();
				Polinom p2=new Polinom();
				imput1=gui.t1.getText();
				imput2=gui.t2.getText();
				p1=Convertire.extract(imput1);
				p2=Convertire.extract(imput2);
				
				if (arg0.getSource().equals(gui.b1))
				{
					rezultat = Operatii.adunare(p1, p2);
					//Collections.sort(rezultat.termeni);
				}
				
				if (arg0.getSource().equals(gui.b2))
				{
					rezultat = Operatii.scadere(p1, p2);
					//Collections.sort(rezultat.termeni);
				}
				if (arg0.getSource().equals(gui.b3))
				{
					rezultat = Operatii.inmultirea(p1, p2);
					//Collections.sort(rezultat.termeni);
				}
				
				if (arg0.getSource().equals(gui.b4)) {
					rezultat = Operatii.derivare(p1);
					//Collections.sort(rezultat.termeni);

				}
				if (arg0.getSource().equals(gui.b5)) {
					rezultat = Operatii.integrare(p1);
					//Collections.sort(rezultat.termeni);
				}
					
				
				updateGui();
				
				if (arg0.getSource().equals(gui.b7)) {
					gui.t1.setText("");
					gui.t2.setText("");
					resetGui();
				}
			}
		};
		gui.b1.addActionListener(act);
		gui.b2.addActionListener(act);
		gui.b3.addActionListener(act);
		gui.b4.addActionListener(act);
		gui.b5.addActionListener(act);
		gui.b6.addActionListener(act);
		gui.b7.addActionListener(act);
	}
	private void resetGui() {
		// TODO Auto-generated method stub
		gui.t3.setText(" ");
		
	}
	public void updateGui() {
		// TODO Auto-generated method stub
		gui.t3.setText(rezultat.toString());
	}
	
	public static void main(String[] args) {
	
		Gui g=new Gui();
		Controller c=new Controller(g);
	}
	}

	