package main;

public class Operatii extends Polinom {

	public static Polinom adunare(Polinom p1, Polinom p2) {
		Polinom rezultat = p1.copie();
		for (Monom m1 : p2.termeni)
			rezultat.addTermeni(m1);
		return rezultat;
	}


	public static Monom inmultire(Monom m1, Monom m2) {
		return new Monom(m1.coeficient*m2.coeficient, m1.putere + m2.putere);
	}
	public static Polinom inmultirea(Polinom p1, Polinom p2) {
		Polinom rezultat = new Polinom();
		Polinom copieP1 = p1.copie();
		Polinom copieP2 = p2.copie();
		for (Monom m1 : copieP1.termeni) {
			for (Monom m2 : copieP2.termeni) {
				rezultat.addTermeni(inmultire(m1, m2));
			}
		}
		rezultat = rezultat.copie();
		return rezultat;
	}

	
	public static Polinom derivare(Polinom p1) {
		Polinom rezultat = new Polinom();
		Polinom copieP1 = p1.copie();
		for (Monom m1 : copieP1.termeni) {
			if (m1.putere>0) {
				m1.coeficient = m1.coeficient*m1.putere;
				m1.putere--;
				rezultat.addTermeni(m1);
			}
			else break;
		}
		return rezultat;
	}
	public static Polinom scadere(Polinom p2, Polinom p1) {
		Polinom rezultat = p2.copie();
		Polinom copieP1 = p1.copie();
		for (Monom m1 : copieP1.termeni) {
			m1.coeficient *= -1;
			rezultat.addTermeni(m1);
		}

		return rezultat;
	}
	
	public static Polinom  integrare(Polinom p1)
	{
		Polinom rezultat = new Polinom();
		for (Monom m1 : p1.termeni) {
			m1.putere++;
			m1.coeficient = m1.coeficient / m1.putere;
			rezultat.addTermeni(m1);
		}
		return rezultat;
	}
}