package main;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

public class Gui {
	JFrame f;
	JLabel l1;
	JLabel l2;
	JLabel l3;
	JLabel l4;
	JTextField t1;
	JTextField t2;
	JTextField t3;
	JButton b1;
	JButton b2;
	JButton b3;
	JButton b4;
	JButton b5;
	JButton b6;
	JButton b7;
	
	public Gui(){
		 f= new JFrame("Calculator de polinoame-Iacob Anca Andreea");
		 l1= new JLabel("Introduceti primul polinom:");
		 t1= new JTextField();
		 l2= new JLabel("Introduceti al doilea  polinom:");
		 t2= new JTextField();
		 l3= new JLabel("Rezultatul este:");
		 t3= new JTextField();
		 l4= new JLabel("Alegeti operatia :");
		 b1=new JButton("Adunare");
		 b2=new JButton("Scadere");
		 b3=new JButton("Inmultire");
		 b4=new JButton("Derivare");
		 b5=new JButton("Integrare");
		 b6=new JButton("Impartire");
		 b7=new JButton("Resetare");
		 
		 try{
			f.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("C:/Users/Andu/Desktop/poli.jpg")))));
		}catch(IOException e)
		{
			System.out.println("Imaginea nu exista");
		}
		
		f.setResizable(false);
		f.pack();
		f.setVisible(true);
		f.setSize(1200, 600);
		f.add(l1);
		l1.setBounds(40,40,200, 80);
		l1.setForeground(Color.BLUE);
		f.add(t1);
		t1.setBounds(40, 120, 200, 40);
		f.add(l2);
		l2.setForeground(Color.blue);
		l2.setBounds(40, 170, 200, 40);
		f.add(t2);
		t2.setBounds(40, 220, 200, 40);
		f.add(l3);
		l3.setForeground(Color.RED);
		l3.setBounds(40, 350, 200, 40);
		f.add(t3);
		t3.setBounds(40,400,200,40);
		f.add(l4);
		l4.setForeground(Color.DARK_GRAY);
		l4.setBounds(950,40,200, 80);
		f.add(b1);
		b1.setBackground(Color.RED);
		b1.setBounds(950, 110, 150, 40);
		f.add(b2);
		b2.setBackground(Color.yellow);
		b2.setBounds(950, 160, 150, 40);
		f.add(b3);
		b3.setBackground(Color.CYAN);
		b3.setBounds(950, 210, 150, 40);
		f.add(b4);
		b4.setBackground(Color.MAGENTA);
		b4.setBounds(950, 260, 150, 40);
		f.add(b5);
		b5.setBackground(Color.ORANGE);
		b5.setBounds(950, 310, 150, 40);
		f.add(b6);
		b6.setBackground(Color.GREEN);
		b6.setBounds(950, 360, 150, 40);
		f.add(b7);
		b7.setForeground(Color.BLUE);
		b7.setBounds(480,500,200,40);
	}
		void setTotal(String Polinomm)
		{
			t3.setText(Polinomm);
		}
		public void showError(String string) {
		
}
	
}
