package testing;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;

import main.Monom;
import main.Operatii;
import main.Polinom;

public class TestIntegrare {


	@Test
	public void test(){
		Polinom x;
		Polinom p1=new Polinom();
		Monom m1=new Monom(3,2);
		Monom m2=new Monom(2,1);
		p1.addTermeni(m1);
		p1.addTermeni(m2);
		x=Operatii.integrare(p1);
		String y=x.toString();
		System.out.println(y);
		assertEquals(" +1.0X3  +1.0X2 ",y);
	}

}
