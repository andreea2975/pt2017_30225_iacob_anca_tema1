package testing;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestAdunare.class, TestDerivare.class, TestInmultire.class, TestIntegrare.class, TestScadere.class })
public class AllTests {

}
