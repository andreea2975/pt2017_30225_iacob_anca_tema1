
package testing;

import static org.junit.Assert.*;

import org.junit.Test;

import main.Monom;
import main.Operatii;
import main.Polinom;

public class TestInmultire {

	@Test
	public void test(){
		Polinom x;
		Polinom p1=new Polinom();
		Polinom p2=new Polinom();
		Monom m1=new Monom(4,2);
		p1.addTermeni(m1);
		Monom m4=new Monom(1,2);
		p2.addTermeni(m4);
		x=Operatii.inmultirea(p1, p2);
		String y=x.toString();
		System.out.println(y);
		assertEquals(" +4.0X4 ",y);
		
		}	


}
