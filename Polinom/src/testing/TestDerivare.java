package testing;

import static org.junit.Assert.*;


import org.junit.Test;

import main.Monom;
import main.Operatii;
import main.Polinom;

public class TestDerivare {

	@Test
	public void test(){
	Polinom x;
	Polinom p1=new Polinom();
	Monom m1=new Monom(4,2);
	Monom m2=new Monom(1,1);
	Monom m3=new Monom(2,0);
	p1.addTermeni(m1);
	p1.addTermeni(m2);
	p1.addTermeni(m3);
	x=Operatii.derivare(p1);
	String y=x.toString();
	System.out.println(y);
	assertEquals(" +8.0X1  +1.0X0 ",y);
	}

}
