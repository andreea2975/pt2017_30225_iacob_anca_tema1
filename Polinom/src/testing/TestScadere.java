package testing;

import static org.junit.Assert.*;

import org.junit.Test;

import main.Monom;
import main.Operatii;
import main.Polinom;

public class TestScadere {

	@Test
	public void test() {
		Polinom x;
		Polinom p1=new Polinom();
		Polinom p2=new Polinom();
		Monom m1=new Monom(4,2);
		Monom m2=new Monom(1,1);
		Monom m3=new Monom(2,0);
		p1.addTermeni(m1);
		p1.addTermeni(m2);
		p1.addTermeni(m3);
		Monom m4=new Monom(1,2);
		Monom m5=new Monom(4,1);
		Monom m6=new Monom(2,0);
		p2.addTermeni(m4);
		p2.addTermeni(m5);
		p2.addTermeni(m6);
		x=Operatii.scadere(p1, p2);
		String y=x.toString();
		System.out.println(y);
		assertEquals(" +3.0X2  -3.0X1  0.0X0 ",y);
	}

}
